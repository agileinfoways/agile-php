<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2012, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
*/
App::import('Vendor','clssmtpmail');
App::uses('AppController', 'Controller');
class UsersController extends AppController {

	/**
	 * Controller name
	 *
	 * @var string
	 */

	public $name = 'Users';

	/**
	 * This controller does not use a model
	 *
	 * @var array
	 */
	public $uses = array('User');
	

	/**
	 * Displays a view
	 *
	 * @param mixed What page to display
	 * @return void
	*/

	function beforeFilter() {
	     parent::beforeFilter();
	}

	// for client login process

	public function login() {

		if($this->Session->read('Auth.User.id'))
		{
			$this->redirect('dashboard');
		}
		$this->layout="login";
		$this->set('title_for_layout','Login');

		if($this->request->data)
		{
				
			$this->User->set($this->data);
		 if($this->User->validates())
		 {

		  if($this->Auth->login()){

		  	 
		  	$id=$this->Auth->user('id');
		  	$this->User->query("UPDATE users SET lastlogin=now() WHERE id=".$id);
		  	$this->redirect('dashboard');

		  }
		  else
		  {
		  	$this->Session->setFlash(__('Invalid email or password, try again'));
		  }
		 }
	 }


	}


	// users dashboard after login
	public function dashboard()
	{     
      $today_task = $this->Systemeventtask->find('first',array('conditions'=>array('Systemeventtask.to_id'=>$this->Auth->user('id'),
                            'Systemeventtask.type'=>'T',
                            'Systemeventtask.status'=>'A','Systemeventtask.start_date >= CURDATE()'),
        'order'=>array('Systemeventtask.start_date')));
        $this->set('today_task',$today_task);

      
	}

	// for users logout
	public function logout()
	{
		$this->Session->setFlash(__('You are successfully logged Out'));
		$this->redirect($this->Auth->logout());

	}

  //users forget password function
  function forgotpassword(){

    $this->layout="login";
 
    if($this->request->data){

      $newpassword = substr(str_shuffle( 'abcdefghijkLuCIDcontactmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$' ), 0, 10);
      $User = $this->User->find('list',array('conditions'=>array('username'=>$this->request->data['username'])));

      if(count($User)==1){

        $data['User']['password'] = $newpassword;
        $this->User->id=key($User);
        if($this->User->save($data)){

          $smtp     = new clssmtpmail();

          $message="Hi,<br><br>Below is your New login credential for Lucid Contact<br><br>Your Email:".$this->request->data['username']."<br><br>Password:".$newpassword."<br><br>Thanks,<br>Lucid Contact Team";
          #$email=$arr3['Client']['clients_email'];

          $smtp->authSendEmail("admin@admin.com","admin",$this->request->data['username'],"Your New Login Credentials Lucid Contact",$message);

          $this->Session->setFlash("New password sent to your e-mail successfully.",'default', array('class' => 'found_date'));
          $this->redirect('login');
        }

      }
      else
        $this->Session->setFlash("Email does not exists.");
    }

  }
	
}
